var Mock = require('mockjs');

module.exports = function() {
  return {
    taskList: Mock.mock({
      "dataList":[
          { 'date': '@datetime', 'name': '@name', 'address': '@region'},
          { 'date': '@datetime', 'name': '@name', 'address': '@region'},
          { 'date': '@datetime', 'name': '@name', 'address': '@region'},
          { 'date': '@datetime', 'name': '@name', 'address': '@region'},
          { 'date': '@datetime', 'name': '@name', 'address': '@region'},
          { 'date': '@datetime', 'name': '@name', 'address': '@region'},
          { 'date': '@datetime', 'name': '@name', 'address': '@region'},
          { 'date': '@datetime', 'name': '@name', 'address': '@region'},
          { 'date': '@datetime', 'name': '@name', 'address': '@region'},
          { 'date': '@datetime', 'name': '@name', 'address': '@region'}],
      "offSet":0,
      "pageSize":10,
      "totalData":21354,
      "currentPage":30,
      "totalPage":123,
      "formQuery":{
        "status":"0",
        "pageSize":10,
        "pageNo":30
      }
    }),
    bizTypeList: Mock.mock({
      "dataList":[
        { 'value': '@INT(1,100)', 'label': '@name'},
        { 'value': '@INT(1,100)', 'label': '@name'},
        { 'value': '@INT(1,100)', 'label': '@name'}
      ]
    }),
    ruleHeadListBybizType: Mock.mock({
      "dataList":[
        {'id':'@INT(1,100)','title':'@name','ynFlag':true},
        {'id':'@INT(1,100)','title':'@name','ynFlag':false},
        {'id':'@INT(1,100)','title':'@name','ynFlag':true}
      ]
    }),
    ruleList: Mock.mock({
      "dataList": [
        {"textEn":"$bizType eq '138'","textCn":"业务类型等于'138'","expType":"0"},
        {"textEn":"$bizType eq '138'","textCn":"业务类型等于'138'","expType":"10"},
        {"textEn":"$bizType eq '138'","textCn":"业务类型等于'138'","expType":"10"},
        {"textEn":"$bizType eq '138'","textCn":"业务类型等于'138'","expType":"10"},
      ]
    })
  }
};
