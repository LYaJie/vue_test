import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/Dashboard'
import CalculatorRules from '@/components/rules/CalculatorRules'

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      name: '/',
      component:Dashboard
    },{
      path: '/dashboard',
      name: '/dashboard',
      component:Dashboard
    },{
      path: '/rules',
      name: '/rules',
      component:CalculatorRules
    },{
      path: '/rules/calculatorRules',
      name: '/rules/calculatorRules',
      component:CalculatorRules
    }
  ]
})
